package com.example.shiftlabtest;

import com.example.shiftlabtest.domain.FormFactor;
import com.example.shiftlabtest.domain.Hdd;
import com.example.shiftlabtest.domain.Pc;
import com.example.shiftlabtest.dto.ProductDto;
import io.restassured.http.ContentType;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ControllerTest {
    @Order(1)
    @Test
    public void canGetAllProducts() {
        given()
                .contentType(ContentType.JSON)
        .when()
                .get("/products")
        .then()
                .statusCode(200)
                .body("$", hasSize(greaterThanOrEqualTo(6))); // Изначально 6, но могут добавиться впоследствии
    }

    @Test
    public void canGetOnlyHdds() {
        ProductDto[] products =
                given()
                    .contentType(ContentType.JSON)
                    .param("type", "hdd")
                .when()
                    .get("/products")
                .then()
                    .statusCode(200)
                    .body("$", hasSize(greaterThanOrEqualTo(2)))
                .extract()
                    .as(ProductDto[].class);
        MatcherAssert.assertThat(Arrays.asList(products),
                everyItem(hasProperty("volume", notNullValue())));
    }

    @Test
    public void canQueryForSingleProduct() {
        given()
                .contentType(ContentType.JSON)
        .when()
                .get("/products/1")
        .then()
                .statusCode(200)
                .body("id", equalTo(1))
                .body("batchNumber", equalTo(123))
                .body("manufacturer", equalTo("abc"))
                .body("cost", equalTo(100.99f))
                .body("quantity", equalTo(10))
                .body("volume", equalTo(500));
    }

    @Test
    public void canUpdateProduct() {
        Hdd hdd = new Hdd();
        hdd.setId(1L);
        hdd.setVolume(2000); // Устанавливаем другое значение
        hdd.setBatchNumber(123L);
        hdd.setManufacturer("abc");
        hdd.setCost(new BigDecimal("100.99"));
        hdd.setQuantity(10);

        given()
                .contentType(ContentType.JSON)
                .body(hdd)
        .when()
                .put("/products/hdd/2")
        .then()
                .statusCode(200)
                .body("id", equalTo(2))
                .body("volume", equalTo(2000));
    }

    @Order(2)
    @Test
    public void canAddNewProduct() {
        Pc pc = new Pc();
        pc.setFormFactor(FormFactor.DESKTOP);
        pc.setBatchNumber(123L);
        pc.setCost(new BigDecimal("123.49"));
        pc.setManufacturer("abcdef");
        pc.setQuantity(10);

        given()
                .contentType(ContentType.JSON)
                .body(pc)
        .when()
                .post("/products/pc")
        .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("formFactor", equalTo("DESKTOP"));
    }
}
