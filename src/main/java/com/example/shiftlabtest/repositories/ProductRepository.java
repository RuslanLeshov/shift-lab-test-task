package com.example.shiftlabtest.repositories;

import com.example.shiftlabtest.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select p from Hdd p")
    List<Hdd> findAllHdd();

    @Query("select p from Laptop p")
    List<Laptop> findAllLaptops();

    @Query("select p from Monitor p")
    List<Monitor> findAllMonitors();

    @Query("select p from Pc p")
    List<Pc> findAllPcs();
}
