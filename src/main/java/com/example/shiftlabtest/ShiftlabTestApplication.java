package com.example.shiftlabtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiftlabTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiftlabTestApplication.class, args);
    }

}
