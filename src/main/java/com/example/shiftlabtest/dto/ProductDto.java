package com.example.shiftlabtest.dto;

import com.example.shiftlabtest.domain.*;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
    private Long id;
    private Long batchNumber;
    private String manufacturer;
    private BigDecimal cost;
    private Integer quantity;

    private Integer volume;
    private Integer size;
    private String diagonal;
    private String formFactor;

    public ProductDto() {
    }

    public ProductDto(Product product) {
        id = product.getId();
        batchNumber = product.getBatchNumber();
        manufacturer = product.getManufacturer();
        cost = product.getCost();
        quantity = product.getQuantity();

        if (product instanceof Hdd) {
            volume = ((Hdd) product).getVolume();
        }

        if (product instanceof Laptop) {
            size = ((Laptop) product).getSize();
        }

        if (product instanceof Monitor) {
            diagonal = ((Monitor) product).getDiagonal();
        }

        if (product instanceof Pc) {
            formFactor = ((Pc) product).getFormFactor().name();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Long batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = diagonal;
    }

    public String getFormFactor() {
        return formFactor;
    }

    public void setFormFactor(String formFactor) {
        this.formFactor = formFactor;
    }
}
