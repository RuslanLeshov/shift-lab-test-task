package com.example.shiftlabtest.controllers;

import com.example.shiftlabtest.dto.ProductDto;
import com.example.shiftlabtest.services.ProductService;
import com.example.shiftlabtest.domain.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("products")
public class ProductController {
    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @PostMapping("/hdd")
    public ProductDto addHdd(@RequestBody Hdd product) {
        return new ProductDto(service.createProduct(product));
    }

    @PostMapping("/laptop")
    public ProductDto addLaptop(@RequestBody Laptop product) {
        return new ProductDto(service.createProduct(product));
    }

    @PostMapping("/monitor")
    public ProductDto addMonitor(@RequestBody Monitor product) {
        return new ProductDto(service.createProduct(product));
    }

    @PostMapping("/pc")
    public ProductDto addPc(@RequestBody Pc product) {
        return new ProductDto(service.createProduct(product));
    }

    @PutMapping("/hdd/{id}")
    public ProductDto updateHdd(@PathVariable("id") Long storedProductId, @RequestBody Hdd product) {
        return new ProductDto(service.updateProduct(product, storedProductId));
    }

    @PutMapping("/laptop/{id}")
    public ProductDto updateLaptop(@PathVariable("id") Long storedProductId, @RequestBody Laptop product) {
        return new ProductDto(service.updateProduct(product, storedProductId));
    }

    @PutMapping("/monitor/{id}")
    public ProductDto updateMonitor(@PathVariable("id") Long storedProductId, @RequestBody Monitor product) {
        return new ProductDto(service.updateProduct(product, storedProductId));
    }

    @PutMapping("/pc/{id}")
    public ProductDto updatePc(@PathVariable("id") Long storedProductId, @RequestBody Pc product) {
        return new ProductDto(service.updateProduct(product, storedProductId));
    }

    @GetMapping
    public List<ProductDto> getAllByType(@RequestParam(required = false) String type) {
        List<? extends Product> list;
        if (StringUtils.isEmpty(type)) {
            list = service.getAll();
        } else {
            list = service.getAllByType(type);
        }

        return list.stream()
                .map(ProductDto::new)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable Long id) {
        return new ProductDto(service.getById(id));
    }

}
