package com.example.shiftlabtest.domain;

import javax.persistence.Entity;

@Entity
public class Monitor extends Product {
    /*
     * По сути диагональ - десятичная дробь,
     * но так как с ней не планируется проводить арифметические операции,
     * то пусть будет строкой, чтоб не решать проблемы точности
     * представления десятичных дробей
     */
    private String diagonal;

    public String getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = diagonal;
    }
}
