package com.example.shiftlabtest.domain;


public enum FormFactor {
    DESKTOP, NETTOP, MONOBLOCK
}
