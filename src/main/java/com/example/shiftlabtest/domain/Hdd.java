package com.example.shiftlabtest.domain;

import javax.persistence.Entity;

@Entity
public class Hdd extends Product {
    private Integer volume;

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }
}
