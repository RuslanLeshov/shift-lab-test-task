package com.example.shiftlabtest.domain;

import javax.persistence.Entity;

@Entity
public class Laptop extends Product {
    private Integer size;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
