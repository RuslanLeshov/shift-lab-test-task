package com.example.shiftlabtest.domain;

import javax.persistence.Entity;
import javax.persistence.Enumerated;

@Entity
public class Pc extends Product {
    @Enumerated
    private FormFactor formFactor;

    public FormFactor getFormFactor() {
        return formFactor;
    }

    public void setFormFactor(FormFactor formFactor) {
        this.formFactor = formFactor;
    }
}
