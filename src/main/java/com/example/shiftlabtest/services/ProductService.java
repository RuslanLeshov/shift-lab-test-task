package com.example.shiftlabtest.services;

import com.example.shiftlabtest.domain.Product;
import com.example.shiftlabtest.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ProductService {
    private final ProductRepository repository;

    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public Product createProduct(Product product) {
        return repository.save(product.getClass().cast(product));
    }

    public Product updateProduct(Product product, Long storedProductId) {
        Product storedProduct = repository.findById(storedProductId).orElse(null);
        if (storedProduct != null) {
            BeanUtils.copyProperties(product, storedProduct, "id");
            return repository.save(storedProduct);
        }
        return null;
    }

    public List<? extends Product> getAllByType(String type) {
        switch (type.toLowerCase()) {
            case "hdd": return repository.findAllHdd();
            case "laptop": return repository.findAllLaptops();
            case "monitor": return repository.findAllMonitors();
            case "pc": return repository.findAllPcs();
            default: return Collections.emptyList();
        }
    }

    public List<Product> getAll() {
        return repository.findAll();
    }

    public Product getById(Long id) {
        return repository.findById(id).orElse(null);
    }

}
