insert into product (id, batch_number, cost, manufacturer, quantity)
values (1, 123, 100.99, 'abc', 10);

insert into product (id, batch_number, cost, manufacturer, quantity)
values (2, 111, 100.99, 'abc', 10);

insert into product (id, batch_number, cost, manufacturer, quantity)
values (3, 222, 100.99, 'abcdef', 10);

insert into product (id, batch_number, cost, manufacturer, quantity)
values (4, 333, 100.99, 'abcdef', 10);

insert into product (id, batch_number, cost, manufacturer, quantity)
values (5, 444, 100.99, 'abc', 10);

insert into product (id, batch_number, cost, manufacturer, quantity)
values (6, 555, 100.99, 'abc', 10);

insert into hdd (id, volume)
values (1, 500);

insert into hdd (id, volume)
values (2, 1000);

insert into laptop (id, size)
values (3, 15);

insert into monitor (id, diagonal)
values (4, '13.8');

insert into pc (id, form_factor)
values (5, 1);

insert into pc (id, form_factor)
values (6, 2);


drop sequence if exists hibernate_sequence;
create sequence hibernate_sequence start with 7 increment by 1;