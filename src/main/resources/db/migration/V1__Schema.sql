create sequence hibernate_sequence start with 1 increment by 1;

create table product
(
    id           bigint not null,
    batch_number bigint,
    cost         numeric(32, 2),
    manufacturer varchar(255),
    quantity     integer,
    primary key (id)
);

create table hdd
(
    id     bigint not null references product(id),
    volume integer,
    primary key (id)
);
create table laptop
(
    id   bigint not null references product(id),
    size integer,
    primary key (id)
);
create table monitor
(
    id       bigint not null references product(id),
    diagonal varchar(10),
    primary key (id)
);
create table pc
(
    id          bigint not null references product(id),
    form_factor integer,
    primary key (id)
);
